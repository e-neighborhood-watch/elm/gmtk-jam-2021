# Dis//connected

*(submitted to the [GMTK Game Jam 2021](https://itch.io/jam/gmtk-2021/rate/1083282))*

In Dis//connected, you control separate puzzle pieces that move in parallel. The goal is to connect them to other pieces in order bridge the gap. Each level is won when you've connected all your pieces. Your controls move all your pieces (and any pieces attached to them) at once, and once a piece is attached there is no way to get it off, so you need to plan and make moves carefully.

### Where to Play the Game

* Gitlab
  * [dev branch pipeline](https://gitlab.com/e-neighborhood-watch/elm/gmtk-jam-2021/-/jobs/artifacts/master/file/build/index.html?job=build) (bleeding edge, debugger on)
  * [pages](https://e-neighborhood-watch.gitlab.io/elm/gmtk-jam-2021/) (most up to date stable release)
* [itch.io](https://e-neighborhood-watch.itch.io/stuck-together)
