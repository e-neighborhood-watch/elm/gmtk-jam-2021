module Color
  exposing
    ( Color
    , getStyle
    )


import Set
  exposing
    ( Set
    )


import Config


type alias Color =
  Int


getStyle : Set Color -> Config.Style
getStyle color =
  if
    Set.member 1 color
  then
    if
      Set.member 2 color
    then
      if
        Set.member 3 color
      then
        Config.players1and2and3
      else
        Config.players1and2
    else
      if
        Set.member 3 color
      then
        Config.players1and3
      else
        Config.player1
  else
    if
      Set.member 2 color
    then
      if
        Set.member 3 color
      then
        Config.players2and3
      else
        Config.player2
    else
      if
        Set.member 3 color
      then
        Config.player3
      else
        Config.noPlayer
