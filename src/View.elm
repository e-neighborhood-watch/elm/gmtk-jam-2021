module View
  exposing
    ( ground
    , block
    )


import Css
import Set
  exposing
    ( Set
    )
import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr


import Color
import Config
import Coupling
import Level.Block
  exposing
    ( Block
    )


block : Set Int -> ( Int, Int ) -> Block -> Svg a
block color ( x, y ) { top, left, bottom, right } =
  let
    xTop : String
    xTop =
      String.fromInt x

    yTop : String
    yTop =
      String.fromInt y

    -- assumes x and y are nonnegative
    centerOfRot : String
    centerOfRot =
      xTop ++ ".5," ++ yTop ++ ".5)"
  in
    Svg.g
      [ SvgAttr.css
        [ Css.property "fill" (Color.getStyle color |> .fill)
        , Css.property "stroke" (Color.getStyle color |> .stroke)
        ]
      ]
      [ Svg.rect
        [ SvgAttr.x (xTop ++ ".1")
        , SvgAttr.y (yTop ++ ".1")
        , SvgAttr.width "0.8"
        , SvgAttr.height "0.8"
        , SvgAttr.stroke "none"
        ]
        []
      , Svg.use
        [ SvgAttr.xlinkHref ("#" ++ Coupling.id right)
        , SvgAttr.x xTop
        , SvgAttr.y yTop
        ]
        []
      , Svg.use
        [ SvgAttr.xlinkHref ("#" ++ Coupling.id bottom)
        , SvgAttr.transform ("rotate(90," ++ centerOfRot)
        , SvgAttr.x xTop
        , SvgAttr.y yTop
        ]
        []
      , Svg.use
        [ SvgAttr.xlinkHref ("#" ++ Coupling.id left)
        , SvgAttr.transform ("rotate(180," ++ centerOfRot)
        , SvgAttr.x xTop
        , SvgAttr.y yTop
        ]
        []
      , Svg.use
        [ SvgAttr.xlinkHref ("#" ++ Coupling.id top)
        , SvgAttr.transform ("rotate(270," ++ centerOfRot)
        , SvgAttr.x xTop
        , SvgAttr.y yTop
        ]
        []
      ]


ground : ( Int, Int ) -> Svg a
ground size =
  List.range 0 (Tuple.first size - 1)
  |> List.concatMap
    ( \x ->
      List.map
        (Tuple.pair x)
        (List.range 0 (Tuple.second size - 1))
    )
  |> List.map ( \ (x,y) ->
    Svg.rect
      [ SvgAttr.x ( (String.fromInt x) ++ ".1")
      , SvgAttr.y ( (String.fromInt y) ++ ".1")
      , SvgAttr.width "0.8"
      , SvgAttr.height "0.8"
      , SvgAttr.stroke Config.background.stroke
      , SvgAttr.strokeWidth "0.05"
      , SvgAttr.fill "none"
      , SvgAttr.rx "0.1"
      , SvgAttr.ry "0.1"
      ]
      []
    )
  |> Svg.g
    []

