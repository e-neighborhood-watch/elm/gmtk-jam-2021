module Coupling
  exposing
    ( Interface (..)
    , canConnect
    , id
    , defs
    )


import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr


type Interface
  = Blank
  | Circle
  | Triangle
  | Square


all : List Interface
all =
  [ Blank
  , Circle
  , Triangle
  , Square
  ]


defs : Svg a
defs =
  all
    |>
      List.concatMap
        ( \ interface ->
          let
            interfaceId : String
            interfaceId =
              id interface
          in
            [ Svg.use
              [ SvgAttr.xlinkHref ("assets/" ++ interfaceId ++ ".svg#content")
              , SvgAttr.id interfaceId
              , SvgAttr.width "1.2"
              , SvgAttr.height "1"
              ]
              []
            ]
        )
    |> Svg.defs []


id : Interface -> String
id interface =
  case
    interface
  of
    Blank ->
      "blank"

    Circle ->
      "symm-circle"

    Triangle ->
      "symm-triangle"

    Square ->
      "symm-square"


canConnect : Interface -> Interface -> Bool
canConnect first second =
  case
    first
  of
    Blank ->
      False
    _ ->
      second == first
