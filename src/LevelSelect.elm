module LevelSelect
  exposing
    ( initLevel
    , selectedLevel
    , update
    , complete
    , setSelectedComplete
    , subscriptions
    , view
    , viewSize
    , Model
    , Message (..)
    , UpdateResult (..)
    , CompleteResult (..)
    )


import Browser.Events
import Css
import Dict
import Json.Decode as Json
import Set
import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Lazy as Svg


import Config
import Config.Keys
import Config.LevelText
import Coupling
import Extra.List as List
import Keys
import Level
import Level.Unloaded as Unloaded
import View


type Message
  = Next
  | Previous
  | Select


type alias LevelOption =
  { completed :
    Bool
  , level :
    Level.Model
  }


type alias Model =
  { selected :
    LevelOption
  , previous :
    List LevelOption
  , remaining :
    List LevelOption
  }


type UpdateResult
  = NoChange
  | Updated Model
  | SelectedLevel Level.Model


type CompleteResult
  = NoMoreLevels Model
  | Advanced Level.Model Model


initLevel : Unloaded.Level -> LevelOption
initLevel unloadedLevel =
  { completed =
    False
  , level =
    Level.load unloadedLevel
  }


selectedLevel : Model -> Level.Model
selectedLevel =
  .selected >> .level


update : Message -> Model -> UpdateResult
update msg prevModel =
  case
    msg
  of
    Select ->
      prevModel
        |> selectedLevel
        |> SelectedLevel

    Next ->
      case
        prevModel.remaining
      of
        [] ->
          NoChange

        newCurrentLevel :: newRemainingLevels ->
          Updated
            { selected =
              newCurrentLevel
            , previous =
              prevModel.selected :: prevModel.previous
            , remaining =
              newRemainingLevels
            }

    Previous ->
      case
        prevModel.previous
      of
        [] ->
          NoChange

        newCurrentLevel :: newPreviousLevels ->
          Updated
            { selected =
              newCurrentLevel
            , previous =
              newPreviousLevels
            , remaining =
              prevModel.selected :: prevModel.remaining
            }


setSelectedComplete : Model -> Model
setSelectedComplete prevModel =
  if
    prevModel.selected.completed
  then
    prevModel
  else
    { selected =
      { completed =
        True
      , level =
        prevModel.selected.level
      }
    , previous =
      prevModel.previous
    , remaining =
      prevModel.remaining
    }


complete : Model -> CompleteResult
complete prevModel =
  case
    prevModel.remaining
  of
    [] ->
      prevModel
        |> setSelectedComplete
        |> NoMoreLevels

    nextLevel :: rest ->
      let
        completedLevel : LevelOption
        completedLevel =
          if
            prevModel.selected.completed
          then
            prevModel.selected
          else
            { completed = True
            , level =
              prevModel.selected.level
            }
      in
        Advanced
          nextLevel.level
          { selected =
            nextLevel
          , previous =
            completedLevel :: prevModel.previous
          , remaining =
            rest
          }


viewOption : Bool -> Int -> Bool -> Svg a
viewOption selected ix completed =
  let
    row =
      ix // Config.levelSelectColumns + 1
  in
    View.block
      ( if
          selected
        then
          Set.fromList [ 1 ]
        else if
          completed
        then
          Set.fromList [ 2, 3 ]
        else
          Set.empty
      )
      ( if
          modBy 2 row == 1
        then
          modBy Config.levelSelectColumns ix + 1
        else
          Config.levelSelectColumns - modBy Config.levelSelectColumns ix
      , row
      )
      { id =
        String.fromInt ix
      , chunkID =
        "0"
      , top =
        if
          List.member
            (modBy (2 * Config.levelSelectColumns) ix)
            [ 0
            , Config.levelSelectColumns
            ]
        then
          Coupling.Triangle
        else
          Coupling.Blank
      , left =
        if
          List.member
            (modBy (2 * Config.levelSelectColumns) ix)
            [ 0
            , 2 * Config.levelSelectColumns - 1
            ]
        then
          Coupling.Blank
        else
          Coupling.Triangle
      , right =
        if
          List.member
            (modBy (2 * Config.levelSelectColumns) ix)
            [ Config.levelSelectColumns - 1
            , Config.levelSelectColumns
            ]
        then
          Coupling.Blank
        else
          Coupling.Triangle
      , bottom =
        if
          List.member
            (modBy (2 * Config.levelSelectColumns) ix)
            [ Config.levelSelectColumns - 1
            , 2 * Config.levelSelectColumns - 1
            ]
        then
          Coupling.Triangle
        else
          Coupling.Blank
      }


viewOptions : LevelOption -> List LevelOption -> List LevelOption -> Svg a
viewOptions selected previous remaining =
  let
    numPrevious : Int
    numPrevious = List.length previous
  in
    List.indexedMap ( \ ix -> .completed >> Svg.lazy2 (viewOption False) (numPrevious - ix - 1)) previous
      ++ Svg.lazy2 (viewOption True) numPrevious selected.completed
      ::
        List.indexedMap ( \ ix -> .completed >> Svg.lazy2 (viewOption False) (numPrevious + ix + 1)) remaining
      |> Svg.g []


levelNumberSize : Int -> ( Int, Int )
levelNumberSize numLevels =
  ( min numLevels Config.levelSelectColumns + 2
  , (numLevels - 1) // Config.levelSelectColumns + 1 + 2
  )


viewSize : Model -> ( Int, Int )
viewSize { previous, remaining } =
  List.length previous + List.length remaining + 1
    |> levelNumberSize


view : Model -> Svg a
view ({ selected, previous, remaining } as model) =
  let
    ( width, height ) =
      viewSize model
  in
  Svg.g
    [ ]
    [ Svg.rect
      [ SvgAttr.x "-0.3"
      , SvgAttr.y "-0.3"
      , SvgAttr.fill Config.background.fill
      , SvgAttr.width ( (String.fromInt width) ++ ".6" )
      , SvgAttr.height ( (String.fromInt height) ++ ".6" )
      , SvgAttr.stroke Config.background.stroke
      , SvgAttr.strokeWidth "0.2"
      ]
      []
    , Svg.lazy View.ground (width, height)
    , Svg.lazy3 viewOptions selected previous remaining
    , Svg.text_
      [ SvgAttr.css
        [ Css.fontSize (Css.px 0.15)
        , Css.fontFamilies [ "Lucida Console", "Courier", "sans-serif" ]
        , Css.property "fill" Config.levelSelectHighlightStroke
        ]
      , SvgAttr.y (String.fromInt height ++ ".1")
      , SvgAttr.x ( String.fromFloat (toFloat (Config.levelSelectColumns + 2) / 2) )
      , SvgAttr.textAnchor "middle"
      ]
      [ Svg.text Config.LevelText.select
      ]
    ]


keyDecoder : String -> Json.Decoder Message
keyDecoder key =
  [ ( Config.Keys.previous
    , Previous
    )
  , ( Config.Keys.next
    , Next
    )
  , ( Config.Keys.select
    , Select
    )
  ]
  |> Dict.fromList
  |> Keys.makeKeyDecoder


keyboardSubscriptions : Sub Message
keyboardSubscriptions =
  Json.string
    |> Json.field "key"
    |> Json.andThen keyDecoder
    |> Browser.Events.onKeyDown


subscriptions : Sub Message
subscriptions =
  keyboardSubscriptions
