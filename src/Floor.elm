module Floor exposing
  (..)

import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr

import Direction exposing
  ( Direction
  )
  
type Floor
  = Slippery
  | Belt Direction

toFilename : Floor -> String
toFilename floor =
  case
    floor
  of
    Slippery ->
      "slippery"
    Belt _ ->
      "we haven't implemented this yet"

all =
  [ Slippery
  ]

defs : Svg a
defs =
  all
    |>
      List.concatMap
        ( \ floorType ->
          let
            floorTypeId : String
            floorTypeId =
              toFilename floorType
          in
            [ Svg.use
              [ SvgAttr.xlinkHref ("assets/" ++ floorTypeId ++ ".svg#content")
              , SvgAttr.id floorTypeId
              , SvgAttr.width "1"
              , SvgAttr.height "1"
              ]
              []
            ]
        )
    |> Svg.defs []
