module Config.Keys
  exposing
    ( ..
    )


up : String
up =
  "w"


down : String
down =
  "s"


left : String
left =
  "a"


right : String
right =
  "d"


restart : String
restart =
  "r"


exit : String
exit =
  "o"


previous : String
previous =
  "a"


next : String
next =
  "d"


select : String
select =
  "p"
