module Config.LevelText
  exposing
    ( ..
    )


import Config.Keys


default : String
default =
  "If you get stuck, press "
  ++ Config.Keys.restart
  ++ " to reset the level, or "
  ++ Config.Keys.exit
  ++ " to return to the level select."


completed : String
completed =
  "Level complete! Hit any movement key to go to the next level."


select : String
select =
  "Select a level using "
  ++ Config.Keys.previous
  ++ " and "
  ++ Config.Keys.next
  ++ ". Press "
  ++ Config.Keys.select
  ++ " to play a level."

