module Level.Unloaded exposing
  ( Level
  , Block
  )


import Dict
  exposing
    ( Dict
    )
import Set
  exposing
    ( Set
    )


import Coupling
import Floor
  exposing
    ( Floor
    )


type alias Block =
  { top :
    Coupling.Interface
  , left :
    Coupling.Interface
  , bottom :
    Coupling.Interface
  , right :
    Coupling.Interface
  , connection :
    Set Int
  }


type alias Level =
  { blocks :
    Dict ( Int, Int ) Block
  , size :
    ( Int, Int )
  , terrain :
    Dict ( Int, Int ) Floor
  , text :
    Maybe String
  , title :
    String
  }

