module Level.Block
  exposing
    ( Block
    , ID
    , getID
    )


import Coupling


type alias ID =
  String


type alias Block =
  { id :
    ID
  , top :
    Coupling.Interface
  , left :
    Coupling.Interface
  , bottom :
    Coupling.Interface
  , right :
    Coupling.Interface
  , chunkID :
    ID
  }


getID : ( Int, Int ) -> String
getID ( x, y ) =
  String.fromInt x ++ "," ++ String.fromInt y
