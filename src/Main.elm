module Main
  exposing
    ( main
    )


import Browser
import Browser.Events
import Css
import Css.Global as Css
import Dict
import Html.Styled as Html
  exposing
    ( Html
    )
import Json.Decode as Json
import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Lazy as Svg


import Audio
import Config
import Config.Keys
import Coupling
import Floor
import Keys
import Level
import Levels.Bend
import Levels.Contraction
import Levels.Eight
import Levels.First
import Levels.Flower
import Levels.Flower2
import Levels.Jigsaw
import Levels.Lock
import Levels.Match
import Levels.Movement
import Levels.SlipIntro
import Levels.SlipField
import Levels.Speed
import Levels.TheGrid
import Levels.Thicket
import Levels.Whirlpool
import Level.Unloaded as Unloaded
import LevelSelect


type State
  = InLevel Level.Model
  | LevelSelect


type alias Model =
  { levelSelect :
    LevelSelect.Model
  , state :
    State
  , audio :
    Audio.Model
  }


type Message
  = Level Level.Message
  | LSelect LevelSelect.Message
  | Audio Audio.Message
  | RestartLevel
  | ExitLevel
  | NewVisibility Browser.Events.Visibility


init : a -> ( Model, Cmd Message )
init _ =
  let
    ( audioModel, audioCmd ) =
      Audio.initialize
  in
    ( { levelSelect =
        { selected =
          LevelSelect.initLevel Levels.First.level
        , previous =
          []
        , remaining =
          [ Levels.Match.level
          , Levels.Bend.level
          , Levels.TheGrid.level
          , Levels.Jigsaw.level
          , Levels.Thicket.level
          , Levels.Flower2.level
          , Levels.Eight.level
          , Levels.Flower.level
          , Levels.Speed.level
          , Levels.Lock.level
          , Levels.SlipIntro.level
          , Levels.Contraction.level
          , Levels.Whirlpool.level
          , Levels.SlipField.level
          , Levels.Movement.level
          ]
            |> List.map LevelSelect.initLevel
        }
        , state =
          LevelSelect
        , audio =
          audioModel
      }
    , audioCmd
    )


update : Message -> Model -> ( Model, Cmd Message )
update message prevModel =
  let
    audioCmd : Cmd a
    audioCmd =
      if
        prevModel.audio.playing
      then
        Cmd.none
      else
        case
          prevModel.audio.initialized
        of
          Nothing ->
            Cmd.none
          Just initialized ->
            Audio.start initialized
  in
  case
    prevModel.state
  of
    LevelSelect ->
      case
        message
      of
        LSelect msg ->
          ( case
              LevelSelect.update msg prevModel.levelSelect
            of
              LevelSelect.NoChange ->
                prevModel

              LevelSelect.Updated newModel ->
                { levelSelect =
                  newModel
                , state =
                  LevelSelect
                , audio =
                  prevModel.audio
                }

              LevelSelect.SelectedLevel level ->
                { levelSelect =
                  prevModel.levelSelect
                , state =
                  InLevel level
                , audio =
                  prevModel.audio
                }
          , audioCmd
          )
        NewVisibility Browser.Events.Visible ->
          ( prevModel
          , Audio.unmute
          )
        NewVisibility Browser.Events.Hidden ->
          ( prevModel
          , Audio.mute
          )
        Audio audioMsg ->
          ( { levelSelect =
              prevModel.levelSelect
            , state =
              prevModel.state
            , audio =
              Audio.update audioMsg prevModel.audio
            }
          , Cmd.none
          )

        _ ->
          ( prevModel
          , audioCmd
          )

    InLevel levelRecord ->
      case
        message
      of
        LSelect _ ->
          ( prevModel
          , Cmd.none
          )
        Level lvlMsg ->
          ( case
              Level.update lvlMsg levelRecord
            of
              Level.NoChange ->
                prevModel

              Level.Finished ->
                case
                  LevelSelect.complete prevModel.levelSelect
                of
                  LevelSelect.NoMoreLevels newLevelSelect ->
                    { levelSelect =
                      newLevelSelect
                    , state =
                      LevelSelect
                    , audio =
                      prevModel.audio
                    }
                  LevelSelect.Advanced nextLevel newLevelSelect ->
                    { levelSelect =
                      newLevelSelect
                    , state =
                      InLevel nextLevel
                    , audio =
                      prevModel.audio
                    }

              Level.Updated newLevel ->
                { levelSelect =
                  prevModel.levelSelect
                , state =
                  InLevel newLevel
                , audio =
                  prevModel.audio
                }
          , audioCmd
          )

        RestartLevel ->
          ( { levelSelect =
              if
                Level.completed levelRecord
              then
                LevelSelect.setSelectedComplete prevModel.levelSelect
              else
                prevModel.levelSelect
            , state =
              prevModel.levelSelect
                |> LevelSelect.selectedLevel
                |> InLevel
            , audio =
              prevModel.audio
            }
          , audioCmd
          )
        ExitLevel ->
          ( { levelSelect =
              if
                Level.completed levelRecord
              then
                LevelSelect.setSelectedComplete prevModel.levelSelect
              else
                prevModel.levelSelect
            , state =
              LevelSelect
            , audio =
              prevModel.audio
            }
          , audioCmd
          )
        Audio audioMsg ->
          ( { levelSelect =
              prevModel.levelSelect
            , state =
              prevModel.state
            , audio =
              Audio.update audioMsg prevModel.audio
            }
          , Cmd.none
          )
        NewVisibility Browser.Events.Visible ->
          ( prevModel
          , Audio.unmute
          )
        NewVisibility Browser.Events.Hidden ->
          ( prevModel
          , Audio.mute
          )


body : Model -> Html Message
body model =
  case
    model.state
  of
    LevelSelect ->
      let
        ( width, height ) =
          LevelSelect.viewSize model.levelSelect
      in
        Svg.svg
          [ SvgAttr.viewBox
            ( "-0.6 -0.6 "
            ++ String.fromInt (width + 1)
            ++ ".2 "
            ++ String.fromInt (height + 1)
            ++ ".2"
            )
          , SvgAttr.preserveAspectRatio "xMidYMid"
          , SvgAttr.css
            [ Css.position Css.absolute
            , Css.top Css.zero
            , Css.left Css.zero
            , Css.width (Css.pct 100)
            , Css.height (Css.pct 100)
            ]
          ]
          [ Coupling.defs
          , Floor.defs
          , Svg.lazy LevelSelect.view model.levelSelect
          ]
    InLevel level ->
      Svg.svg
        [ SvgAttr.viewBox
          ( "-0.6 -1.6 "
          ++ String.fromInt (Tuple.first level.size + 1)
          ++ ".2 "
          ++ String.fromInt (Tuple.second level.size + 3)
          ++ ".2"
          )
        , SvgAttr.preserveAspectRatio "xMidYMid"
        , SvgAttr.css
          [ Css.position Css.absolute
          , Css.top Css.zero
          , Css.left Css.zero
          , Css.width (Css.pct 100)
          , Css.height (Css.pct 100)
          ]
        ]
        [ Coupling.defs
        , Floor.defs
        , Svg.lazy Level.view level
        ]


globalStyles : List Css.Snippet
globalStyles =
  [ Css.body
    [ Css.backgroundColor (Css.hex Config.pageFill)
    ]
  ]


view : Model -> Browser.Document Message
view model =
  { title =
    "Joined Together"
  , body =
    [ Css.global globalStyles
    , body model
    ]
      |> List.map Html.toUnstyled
  }


keyDecoder : Json.Decoder Message
keyDecoder =
  [ ( Config.Keys.restart
    , RestartLevel
    )
  , ( Config.Keys.exit
    , ExitLevel
    )
  ]
  |> Dict.fromList
  |> Keys.makeKeyDecoder


subscriptions : Model -> Sub Message
subscriptions model =
  Sub.batch <|
    case
      model.state
    of
      InLevel _ ->
        [ Sub.map Level Level.subscriptions
        , Browser.Events.onKeyDown keyDecoder
        , Browser.Events.onVisibilityChange NewVisibility
        , Sub.map Audio Audio.subscriptions
        ]
      LevelSelect ->
        [ Sub.map LSelect LevelSelect.subscriptions
        , Browser.Events.onVisibilityChange NewVisibility
        , Sub.map Audio Audio.subscriptions
        ]


main : Program () Model Message
main =
  Browser.document
    { init =
      init
    , update =
      update
    , view =
      view
    , subscriptions =
      subscriptions
    }
