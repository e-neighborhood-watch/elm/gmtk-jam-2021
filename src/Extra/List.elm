module Extra.List exposing
  ( traverseMaybe
  , splitAndReverse
  )


traverseMaybe : List (Maybe a) -> Maybe (List a)
traverseMaybe maybes =
  case
    maybes
  of
    [] ->
      Just []
    m :: tail ->
      tail
        |> traverseMaybe
        |> Maybe.map2 (::) m


splitAndReverseHelper : List a -> Int -> List a -> ( List a, List a )
splitAndReverseHelper reversed numLeft list =
  if
    numLeft <= 0
  then
    ( reversed, list )
  else
    case
      list
    of
      [] ->
        ( reversed, [] )

      x :: xs ->
        splitAndReverseHelper (x :: reversed) (numLeft - 1) xs


-- Take the first n elements off a list, return them in reverse order
-- along with the remainder of the list
splitAndReverse : Int -> List a -> ( List a, List a )
splitAndReverse =
  splitAndReverseHelper []
