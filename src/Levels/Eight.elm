module Levels.Eight
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "8"
  , size =
    ( 5, 5 )
  , blocks =
    Dict.fromList
    [ ( ( 1, 3 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Blank
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Triangle
        , connection =
          Set.fromList [1]
        }
      )
    , ( ( 3, 1 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Circle
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Blank
        , connection =
          Set.fromList [2]
        }
      )
    , ( ( 2, 2 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Circle
        , right =
          Coupling.Circle
        , bottom =
          Coupling.Circle
        , connection =
          Set.empty
        }
      )
    , ( ( 3, 3 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Circle
        , right =
          Coupling.Circle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 2, 4 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Triangle
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 3, 4 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Triangle
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 1, 1 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Blank
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Triangle
        , connection =
          Set.empty
        }
      )
    , ( ( 2, 0 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Blank
        , right =
          Coupling.Circle
        , bottom =
          Coupling.Circle
        , connection =
          Set.empty
        }
      )
    , ( ( 3, 2 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Blank
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Circle
        , connection =
          Set.empty
        }
      )
    ]
    , terrain =
      Dict.empty
    , text =
      Nothing
  }
