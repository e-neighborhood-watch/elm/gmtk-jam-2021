module Levels.Speed
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Forked Path"
  , size =
    ( 7, 8 )
  , blocks =
    Dict.fromList
      [ ( ( 3, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 1
          }
        )
      , ( ( 3, 2 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Triangle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Triangle
          , connection =
            Set.singleton 2
          }
        )
      , ( ( 4, 5 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 3
          }
        )
      , ( ( 2, 6 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 0 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 5, 0 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 5, 4 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.empty
    , text =
      Nothing
  }
