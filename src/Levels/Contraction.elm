module Levels.Contraction
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Floor
  exposing
    ( Floor
    )
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Contraction"
  , size =
    (7, 6)
  , blocks =
    Dict.fromList
      [ ( ( 1, 2 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 5, 2 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Circle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 3, 1 )
        , { top =
            Coupling.Blank
          , right =
            Coupling.Circle
          , left =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      ]
  , terrain =
    Dict.fromList
      [ ( ( 1, 3 )
        , Floor.Slippery
        )
      , ( ( 5, 3 )
        , Floor.Slippery
        )
      ]
  , text =
    Nothing
  }

