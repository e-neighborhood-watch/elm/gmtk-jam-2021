module Levels.Flower2
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Diamond"
  , size =
    ( 7, 7 )
  , blocks =
    Dict.fromList
      [ ( ( 3, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 5, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 1
          }
        )
      , ( ( 2, 4 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 2
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 4 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 1, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 3, 5 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 2, 2 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 2 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.empty
    , text =
      Nothing
  }
