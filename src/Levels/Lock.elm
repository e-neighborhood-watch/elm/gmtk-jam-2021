module Levels.Lock
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )
import Floor
  exposing
    ( Floor
    )


topBar =
  [ ( ( 0, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Blank
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 1, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 2, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 3, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 4, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Circle
      , connection =
        Set.empty
      }
    )
  , ( ( 5, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 6, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 7, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Circle
      , connection =
        Set.empty
      }
    )
  , ( ( 8, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 9, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Circle
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 10, 0)
    , { top =
        Coupling.Blank
      , left =
        Coupling.Circle
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  ]


mechanism =
  [ ( ( 0, 3)
    , { top =
        Coupling.Square
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 1, 3)
    , { top =
        Coupling.Triangle
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 2, 3)
    , { top =
        Coupling.Circle
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 3, 3)
    , { top =
        Coupling.Square
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 4, 3)
    , { top =
        Coupling.Triangle
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 5, 3)
    , { top =
        Coupling.Triangle
      , left =
        Coupling.Blank
      , right =
        Coupling.Square
      , bottom =
        Coupling.Triangle
      , connection =
        Set.empty
      }
    )
  , ( ( 6, 3)
    , { top =
        Coupling.Square
      , left =
        Coupling.Square
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 7, 3)
    , { top =
        Coupling.Square
      , left =
        Coupling.Blank
      , right =
        Coupling.Triangle
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 8, 3)
    , { top =
        Coupling.Circle
      , left =
        Coupling.Triangle
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 9, 3)
    , { top =
        Coupling.Circle
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  , ( ( 10, 3)
    , { top =
        Coupling.Square
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Square
      , connection =
        Set.empty
      }
    )
  ]


players =
  [ ( ( 2, 5 )
    , { top =
        Coupling.Triangle
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Square
      , connection =
        Set.singleton 1
      }
    )
  , ( ( 7, 5 )
    , { top =
        Coupling.Square
      , left =
        Coupling.Blank
      , right =
        Coupling.Blank
      , bottom =
        Coupling.Blank
      , connection =
        Set.singleton 2
      }
    )
  ]


level : Level
level =
  { title =
    "Lock"
  , size =
    (11, 6)
  , blocks =
    topBar
    ++ mechanism
    ++ players
    |> Dict.fromList
  , terrain =
    Dict.empty
  , text =
    Nothing
  }
