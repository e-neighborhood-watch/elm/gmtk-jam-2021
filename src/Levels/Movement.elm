module Levels.Movement
  exposing
    ( level
    )


import Dict
import Set


import Config.Keys
import Coupling
import Floor
  exposing
    ( Floor
    )
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Movement"
  , size =
    (5, 5)
  , blocks =
    Dict.fromList
      [ ( ( 1, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 4, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Square
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 2, 1 )
        , { top =
            Coupling.Blank
          , right =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 3, 2 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Circle
          , right =
            Coupling.Square
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [1]
          }
        )
      ]
    , terrain =
      Dict.fromList
        [ ( ( 2, 2 )
          , Floor.Slippery
          )
        ]
    , text =
      Just "All colored tiles must be connected before a level is won."
  }

