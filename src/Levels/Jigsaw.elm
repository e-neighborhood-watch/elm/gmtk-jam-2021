module Levels.Jigsaw
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Jigsaw"
  , size =
    (6, 8)
  , blocks =
    Dict.fromList
      [ ( ( 3, 1 )
        , { top =
            Coupling.Square
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 4, 1 )
        , { top =
            Coupling.Circle
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )

      , ( ( 0, 0 )
        , { top =
            Coupling.Circle
          , right =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , bottom =
            Coupling.Circle
          , connection =
            Set.empty
          }
        )
      , ( ( 2, 2 )
        , { top =
            Coupling.Square
          , left =
            Coupling.Square
          , right =
            Coupling.Square
          , bottom =
            Coupling.Square
          , connection =
            Set.empty
          }
        )
      , ( ( 1, 3 )
        , { top =
            Coupling.Square
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Square
          , left =
            Coupling.Triangle
          , right =
            Coupling.Square
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 0, 4 )
        , { top =
            Coupling.Circle
          , right =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , bottom =
            Coupling.Circle
          , connection =
            Set.empty
          }
        )
      , ( ( 1, 5 )
        , { top =
            Coupling.Circle
          , right =
            Coupling.Blank
          , left =
            Coupling.Blank
          , bottom =
            Coupling.Square
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.empty
    , text =
      Nothing
  }

