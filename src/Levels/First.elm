module Levels.First
  exposing
    ( level
    )


import Dict
import Set


import Config.Keys
import Coupling
import Level.Unloaded
  exposing
    ( Level
    )
import Floor
  exposing
    ( Floor)


level : Level
level =
  { title =
    "Beginnings"
  , size =
    (6, 5)
  , blocks =
    Dict.fromList
      [ ( ( 1, 2 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 2, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 2 )
        , { top =
            Coupling.Blank
          , right =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.empty
    , text =
      "The red and blue blocks move together. Try to connect them. (Move with "
      ++ Config.Keys.up
      ++ Config.Keys.left
      ++ Config.Keys.down
      ++ Config.Keys.right
      ++ ")"
      |> Just
  }

