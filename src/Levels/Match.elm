module Levels.Match
  exposing
    ( level
    )


import Dict
import Set


import Config.Keys
import Coupling
import Level.Unloaded
  exposing
    ( Level
    )
import Floor
  exposing
    ( Floor
    )


level : Level
level =
  { title =
    "Match"
  , size =
    (6, 5)
  , blocks =
    Dict.fromList
      [ ( ( 1, 2 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 2, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Square
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 2 )
        , { top =
            Coupling.Blank
          , right =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Circle
          , right =
            Coupling.Square
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.empty
    , text =
      Just "You must match the shapes of the pieces in order for them to connect."
  }

