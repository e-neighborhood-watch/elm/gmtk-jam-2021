module Levels.Bend
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Around the Bend"
  , size =
    ( 7, 5 )
  , blocks =
    Dict.fromList
    [ ( ( 0, 1 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Blank
        , right =
          Coupling.Circle
        , bottom =
          Coupling.Blank
        , connection =
          Set.fromList [1]
        }
      )
    , ( ( 0, 2 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Blank
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Blank
        , connection =
          Set.fromList [2]
        }
      )
    , ( ( 2, 2 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Triangle
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Circle
        , connection =
          Set.empty
        }
      )
    , ( ( 2, 4 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Triangle
        , right =
          Coupling.Circle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 3, 4 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Circle
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 5, 0 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Triangle
        , right =
          Coupling.Circle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 5, 2 )
      , { top =
          Coupling.Circle
        , left =
          Coupling.Circle
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    ]
    , terrain =
      Dict.empty
    , text =
      Nothing
  }
