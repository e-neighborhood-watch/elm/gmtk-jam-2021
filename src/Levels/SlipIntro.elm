module Levels.SlipIntro exposing
  ( level
  )


import Dict
import Set


import Coupling
import Floor
  exposing
    ( Floor
    )
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Slippery When Wet"
  , size =
    (5, 5)
  , blocks =
    Dict.fromList
      [ ( ( 1, 1 )
        , { top =
            Coupling.Circle
          , left =
            Coupling.Circle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Circle
          , connection =
            Set.fromList [1]
          }
        )
      ]
  , terrain =
    Dict.fromList
      [ ( ( 2, 2 )
        , Floor.Slippery
        )
      ]
  , text =
    Just "Anything that moves onto the small circle will move an additional space."
  }

