module Levels.Flower exposing
  ( level
  )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )

level : Level
level =
  { title =
    "Flower"
  , size =
    ( 7, 7 )
  , blocks =
    Dict.fromList
      [ ( ( 3, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.fromList [1]
          }
        )
      , ( ( 5, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [2]
          }
        )
      , ( ( 2, 4 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.fromList [3]
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 4 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 1, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 3, 5 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 2, 2 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 4, 2 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.empty
    , text =
      Just "This level has 3 colors. All three of them must be joined."
  }
