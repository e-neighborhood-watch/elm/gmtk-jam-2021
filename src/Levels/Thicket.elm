module Levels.Thicket
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Thicket"
  , size =
    ( 5, 5 )
  , blocks =
    Dict.fromList
    [ ( ( 2, 1 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Triangle
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Triangle
        , connection =
          Set.fromList [1]
        }
      )
    , ( ( 4, 3 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Triangle
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Blank
        , connection =
          Set.fromList [2]
        }
      )
    , ( ( 3, 2 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Triangle
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Triangle
        , connection =
          Set.empty
        }
      )
    , ( ( 2, 3 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Blank
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 2, 4 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Blank
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 0, 1 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Blank
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Triangle
        , connection =
          Set.empty
        }
      )
    , ( ( 0, 2 )
      , { top =
          Coupling.Triangle
        , left =
          Coupling.Blank
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    , ( ( 1, 0 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Blank
        , right =
          Coupling.Triangle
        , bottom =
          Coupling.Triangle
        , connection =
          Set.empty
        }
      )
    , ( ( 0, 4 )
      , { top =
          Coupling.Blank
        , left =
          Coupling.Blank
        , right =
          Coupling.Blank
        , bottom =
          Coupling.Blank
        , connection =
          Set.empty
        }
      )
    ]
    , terrain =
      Dict.empty
    , text =
      Nothing
  }
