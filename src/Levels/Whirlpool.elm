module Levels.Whirlpool
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Floor
  exposing
    ( Floor
    )
import Level.Unloaded
  exposing
    ( Level
    )


level : Level
level =
  { title =
    "Whirlpool"
  , size =
    ( 7, 7 )
  , blocks =
    Dict.fromList
      [ ( ( 1, 5 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Circle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 5, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Circle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 1
          }
        )
      , ( ( 3, 5 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 2
          }
        )
      , ( ( 5, 5 )
        , { top =
            Coupling.Circle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Triangle
          , connection =
            Set.empty
          }
        )
      , ( ( 1, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Triangle
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 1, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Triangle
          , bottom =
            Coupling.Circle
          , connection =
            Set.empty
          }
        )
      , ( ( 3, 1 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.empty
          }
        )
      , ( ( 5, 1 )
        , { top =
            Coupling.Triangle
          , left =
            Coupling.Blank
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Circle
          , connection =
            Set.empty
          }
        )
      ]
    , terrain =
      Dict.fromList
        [ ( ( 3, 3 )
          , Floor.Slippery
          )
        ]
    , text =
      Nothing
  }
