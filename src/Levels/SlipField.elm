module Levels.SlipField
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Floor
  exposing
    ( Floor
    )
import Level.Unloaded
  exposing
    ( Level
    )


size : (Int, Int)
size =
  ( 6, 6 )


level : Level
level =
  { title =
    "Slip Field"
  , size =
    size
  , blocks =
    Dict.fromList
      [ ( ( 1, 1 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Circle
          , right =
            Coupling.Blank
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 2
          }
        )
      , ( ( 3, 3 )
        , { top =
            Coupling.Blank
          , left =
            Coupling.Blank
          , right =
            Coupling.Circle
          , bottom =
            Coupling.Blank
          , connection =
            Set.singleton 1
          }
        )
      ]
  , terrain =
    List.concatMap
    ( \ x ->
      List.map
        ( \ y ->
          ( ( x, y )
          , Floor.Slippery
          )
        )
        ( List.range
          0
          ( Tuple.second size - 1)
        )
    )
    ( List.range
      0
      ( Tuple.first size - 1)
    )
    |> Dict.fromList
    |> Dict.remove ( 2, 2 )
  , text =
    Just "A puzzle piece can only use a slip tile to move one extra space in a turn."
  }

