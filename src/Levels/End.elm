module Levels.End
  exposing
    ( level
    )


import Dict
import Set


import Coupling
import Level.Unloaded
  exposing
    ( Level
    )
import Floor
  exposing
    ( Floor
    )


level : Level
level =
  { title =
    "End"
  , size =
    (1, 1)
  , blocks =
    Dict.empty
  , terrain =
    Dict.empty
  , text =
    Just "This is the end of the game."
  }

