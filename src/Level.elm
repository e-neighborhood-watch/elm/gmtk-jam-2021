module Level
  exposing
    ( view
    , subscriptions
    , update
    , completed
    , Message
    , UpdateResult (..)
    , Model
    , load
    )


import Browser.Events
import Css
import Dict
  exposing
    ( Dict
    )
import Json.Decode as Json
import Set
  exposing
    ( Set
    )
import Svg.Styled as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Keyed as SvgKeyed
import Svg.Styled.Lazy as Svg


import Level.Block as Block
  exposing
    ( Block
    )
import Config
import Config.Keys
import Config.LevelText
import Coupling
import Direction
  exposing
    ( .. )
import Extra.List as List
import Floor
  exposing
    ( Floor
    )
import Keys
import Level.Unloaded as Unloaded
import View


type Message
  = Move Direction


type UpdateResult
  = NoChange
  | Finished
  | Updated Model


type alias Model =
  { blocks :
    Dict ( Int, Int ) Block
  , colorMap :
    Dict Block.ID (Set Int)
  , size :
    ( Int, Int )
  , terrain :
    Dict ( Int, Int ) Floor
  , text :
    String
  , title :
    String
  }


loadBlock : ( Int, Int ) -> Unloaded.Block -> Block
loadBlock location { top, left, bottom, right, connection } =
  { id =
    Block.getID location
  , top =
    top
  , left =
    left
  , bottom =
    bottom
  , right =
    right
  , chunkID =
    Block.getID location
  }


load : Unloaded.Level -> Model
load { blocks, size, terrain, text, title } =
  { blocks =
    Dict.map loadBlock blocks
  , colorMap =
    blocks
    |> Dict.toList
    |> List.foldl
       ( \ ( location, { connection } ) colorMap ->
         if
           connection /= Set.empty
         then
           Dict.insert (Block.getID location) connection colorMap
         else
           colorMap
       )
       Dict.empty
  , text =
    case
      text
    of
      Nothing ->
        Config.LevelText.default
      Just t ->
        t
  , size =
    size
  , terrain =
    terrain
  , title =
    title
  }


completed : Model -> Bool
completed level =
  level.colorMap
  |> Dict.size
  |> (>=) 1


move : ( Int, Int ) -> Direction -> ( Int, Int ) -> Maybe ( Int, Int )
move size dir ( x, y ) =
  case
    dir
  of
    Up ->
      if
        y > 0
      then
        Just ( x, y - 1 )
      else
        Nothing

    Down ->
      if
        y < Tuple.second size - 1
      then
        Just ( x, y + 1 )
      else
        Nothing

    Left ->
      if
        x > 0
      then
        Just ( x - 1, y )
      else
        Nothing

    Right ->
      if
        x < Tuple.first size - 1
      then
        Just ( x + 1, y )
      else
        Nothing


handleTerrain : ( Int, Int ) -> Direction -> Dict ( Int, Int ) Block -> Dict ( Int, Int ) Floor -> Dict ( Int, Int ) Block
handleTerrain size dir blocks terrains =
  moveBlocks
    size
    dir
    ( \ block ->
      List.member
        block.chunkID
          ( Dict.filter
            ( \ _ terrain ->
              terrain == Floor.Slippery
            )
            terrains
          |> Dict.keys
          |> List.filterMap ( \loc -> Dict.get loc blocks )
          |> List.map .chunkID
          )
    )
    blocks
  |> Maybe.withDefault blocks


moveBlocks : ( Int, Int ) -> Direction -> ( Block -> Bool ) -> (Dict ( Int, Int ) Block) -> Maybe (Dict ( Int, Int ) Block)
moveBlocks size dir predicate blocks =
  Dict.toList blocks
  |> List.map (
    \( (x,y), block ) ->
      if
        predicate block
      then
        Maybe.map
        ( \pos -> (pos, block) )
        ( move size dir (x,y) )
      else
        Just ( (x,y), block )
  )
  |> List.traverseMaybe
  |>
    Maybe.andThen
      ( \ newBlocksList ->
        let
          newBlocks : Dict ( Int, Int ) Block
          newBlocks =
            Dict.fromList newBlocksList
        in
          if
            List.length newBlocksList > Dict.size newBlocks
          then
            Nothing
          else
            Just newBlocks
      )


setConnection : ( Int, Int ) -> Coupling.Interface -> ( Block -> Coupling.Interface ) -> Dict ( Int, Int ) Block -> List ( Int, Int ) -> Block.ID -> ( List ( Int, Int ), Dict ( Int, Int ) Block )
setConnection adjacentLoc side adjacentSide blocks rest chunkID =
  case
    Dict.get adjacentLoc blocks
  of
    Nothing ->
      ( rest, blocks )

    Just adjacentBlock ->
      if
        Coupling.canConnect side (adjacentSide adjacentBlock)
          && chunkID < adjacentBlock.chunkID
      then
        ( adjacentLoc :: rest
        , Dict.insert
          adjacentLoc
          { top =
            adjacentBlock.top
          , bottom =
            adjacentBlock.bottom
          , left =
            adjacentBlock.left
          , right =
            adjacentBlock.right
          , id =
            adjacentBlock.id
          , chunkID =
            chunkID
          }
          blocks
        )
      else
        ( rest, blocks )


-- This traverses the potential blocks to connect using a depth first
-- traversal. The first argument is the stack, and the connected status
-- of each block is used to check if it has been visited
updateConnections : List ( Int, Int ) -> Dict ( Int, Int ) Block -> Dict ( Int, Int ) Block
updateConnections activeConnected blocks =
  case
    activeConnected
  of
    [] ->
      blocks

    nextBlock :: rest ->
      case
        Dict.get nextBlock blocks
      of
        Nothing ->
          updateConnections rest blocks

        Just { top, bottom, left, right, chunkID } ->
          let
            nextBlockX : Int
            nextBlockX =
              Tuple.first nextBlock

            nextBlockY : Int
            nextBlockY =
              Tuple.second nextBlock

            ( activeConnectedWithAbove, blocksWithAbove ) =
              let
                aboveLoc : ( Int, Int )
                aboveLoc = ( nextBlockX, nextBlockY - 1 )
              in
                setConnection aboveLoc top .bottom blocks rest chunkID

            ( activeConnectedWithAboveBelow, blocksWithAboveBelow ) =
              let
                belowLoc : ( Int, Int )
                belowLoc = ( nextBlockX, nextBlockY + 1 )
              in
                setConnection belowLoc bottom .top blocksWithAbove activeConnectedWithAbove chunkID

            ( activeConnectedWithAboveBelowLeft, blocksWithAboveBelowLeft ) =
              let
                leftLoc : ( Int, Int )
                leftLoc = ( nextBlockX - 1, nextBlockY )
              in
                setConnection leftLoc left .right blocksWithAboveBelow activeConnectedWithAboveBelow chunkID

            ( activeConnectedFinal, blocksFinal ) =
              let
                rightLoc : ( Int, Int )
                rightLoc = ( nextBlockX + 1, nextBlockY )
              in
                setConnection rightLoc right .left blocksWithAboveBelowLeft activeConnectedWithAboveBelowLeft chunkID
          in
            updateConnections activeConnectedFinal blocksFinal


updateColors : Dict ( Int, Int ) Block -> Dict Block.ID (Set Int) -> Dict Block.ID (Set Int)
updateColors blocks colorMap =
  colorMap
  |> Dict.toList
  |> List.foldl
     ( \ ( coloredChunkID, colors) partialColorMap ->
        case
          Dict.values blocks
          |> List.filter ( .id >> (==) coloredChunkID )
        of
          [ source ] ->
            if
              source.chunkID == source.id
            then
              partialColorMap
            else
              partialColorMap
              |> Dict.update
                 source.chunkID
                 ( Maybe.withDefault Set.empty
                 >> ( Set.union colors )
                 >> Just
                 )
              |> Dict.remove
                 coloredChunkID
          _ ->
            partialColorMap
     )
     colorMap


connectedToPlayer : Model -> Block -> Bool
connectedToPlayer model { chunkID } =
  case
    Dict.get chunkID model.colorMap
  of
    Nothing ->
      False
    Just s ->
      s /= Set.empty


update : Message -> Model -> UpdateResult
update msg prevModel =
  case
    msg
  of
    Move direction ->
      if
        completed prevModel
      then
        Finished
      else
        case
          moveBlocks
            prevModel.size
            direction
            ( connectedToPlayer prevModel )
            prevModel.blocks
        of
          Nothing ->
            NoChange
          Just steppedBlocks ->
            let
              terrainHandled : Dict ( Int, Int ) Block
              terrainHandled =
                handleTerrain prevModel.size direction steppedBlocks prevModel.terrain

              newBlocks : Dict ( Int, Int ) Block
              newBlocks =
                  updateConnections
                    ( Dict.keys
                      terrainHandled
                    )
                    terrainHandled
            in
              Updated
                { blocks =
                  newBlocks
                , colorMap =
                  updateColors
                    newBlocks
                    prevModel.colorMap
                , size =
                  prevModel.size
                , terrain =
                  prevModel.terrain
                , text =
                  prevModel.text
                , title =
                  prevModel.title
                }


viewFloor : Dict ( Int, Int ) Floor -> Svg a
viewFloor terrains =
  List.map
    ( \( (x, y), floor ) ->
      Svg.use
        [ SvgAttr.x (String.fromInt x)
        , SvgAttr.y (String.fromInt y)
        , SvgAttr.xlinkHref ("#" ++ Floor.toFilename floor)
        ]
        []
    )
    (Dict.toList terrains)
  |> Svg.g
    []


viewBlocks : Dict Block.ID (Set Int) -> Dict ( Int, Int ) Block -> Svg a
viewBlocks colorMap =
  Dict.map
    ( \ k v ->
      let
        color =
          Dict.get v.chunkID colorMap
          |> Maybe.withDefault Set.empty
      in
        ( v.id
        , Svg.lazy3 View.block color k v
        )
    )
    >> Dict.values
    >> SvgKeyed.node
      "g"
      []


view : Model -> Svg a
view ({ blocks, colorMap, size, terrain, text, title } as model) =
  Svg.g
    [ ]
    [ Svg.text_
      [ SvgAttr.css
        [ Css.fontSize (Css.px 0.8)
        , Css.fontFamilies [ "Lucida Console", "Courier", "sans-serif" ]
        ]
      , SvgAttr.y "-0.7"
      , SvgAttr.x ( String.fromFloat ( toFloat (Tuple.first size) / 2) )
      , SvgAttr.textAnchor "middle"
      , if
          completed model
        then
          SvgAttr.fill Config.levelWin
        else
          SvgAttr.fill Config.background.stroke
      ]
      [ Svg.text title
      ]
    , Svg.rect
      [ SvgAttr.x "-0.3"
      , SvgAttr.y "-0.3"
      , SvgAttr.fill Config.background.fill
      , SvgAttr.width ( (String.fromInt (Tuple.first size)) ++ ".6" )
      , SvgAttr.height ( (String.fromInt (Tuple.second size)) ++ ".6" )
      , SvgAttr.stroke Config.background.stroke
      , SvgAttr.strokeWidth "0.2"
      ]
      []
    , Svg.lazy View.ground size
    , Svg.lazy viewFloor terrain
    , Svg.lazy2 viewBlocks colorMap blocks
    , Svg.text_
      [ SvgAttr.css
        [ Css.fontSize (Css.px 0.2)
        , Css.fontFamilies [ "Lucida Console", "Courier", "sans-serif" ]
        ]
      , SvgAttr.y ( String.fromInt (Tuple.second size + 1) )
      , SvgAttr.x ( String.fromFloat ( toFloat (Tuple.first size) / 2 ) )
      , SvgAttr.textAnchor "middle"
      -- Uncommenting this would constrain text to be within the width of the SVG
      -- viewBox. Unfortunately doing so makes the text look bad
      --, SvgAttr.textLength ( (String.fromInt (Tuple.first size + 1)) ++ ".2" )
      , if
          completed model
        then
          SvgAttr.fill Config.levelWin
        else
          SvgAttr.fill Config.background.stroke
      ]
      [ Svg.text
        ( if
            completed model
          then
            Config.LevelText.completed
          else
            text
        )
      ]
    ]


keyDecoder : String -> Json.Decoder Direction
keyDecoder key =
  [ ( Config.Keys.up
    , Up
    )
  , ( Config.Keys.left
    , Left
    )
  , ( Config.Keys.down
    , Down
    )
  , ( Config.Keys.right
    , Right
    )
  ]
  |> Dict.fromList
  |> Keys.makeKeyDecoder


keyboardSubscriptions : Sub Direction
keyboardSubscriptions =
  Json.string
    |> Json.field "key"
    |> Json.andThen keyDecoder
    |> Browser.Events.onKeyDown


subscriptions : Sub Message
subscriptions =
  Sub.map Move keyboardSubscriptions
