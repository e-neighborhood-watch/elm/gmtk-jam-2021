module Config
  exposing
    ( ..
    )


type alias Style =
  { fill :
    String
  , stroke :
    String
  }


player1 : Style
player1 =
  { fill =
    "#cf0084"
  , stroke =
    "#ff2eb3"
  }


player2 : Style
player2 =
  { fill =
    "#0085db"
  , stroke =
    "#28abff"
  }


player3 : Style
player3 =
  { fill =
    "#cfcf34"
  , stroke =
    "#efef55"
  }


players1and2 : Style
players1and2 =
  { fill =
    "#5400e5"
  , stroke =
    "#711fff"
  }


players1and3 : Style
players1and3 =
  { fill =
    "#c21600"
  , stroke =
    "#ff533d"
  }


players2and3 : Style
players2and3 =
  { fill =
    "#15b125"
  , stroke =
    "#55d555"
  }


players1and2and3 : Style
players1and2and3 =
  { fill =
    "#151415"
  , stroke =
    "#3a3b3a"
  }


noPlayer : Style
noPlayer =
  { fill =
    "#bbbbbb"
  , stroke =
    "#dcdcdc"
  }


background : Style
background =
  { fill =
    "#333333"
  , stroke =
    "#555555"
  }


pageFill : String
pageFill =
  "#1a1a1a"


levelSelectOpen : Style
levelSelectOpen =
  players1and2


levelSelectComplete : Style
levelSelectComplete =
  players2and3


levelSelectHighlightStroke : String
levelSelectHighlightStroke =
  "#dcdcdc"


levelSelectColumns : Int
levelSelectColumns =
  5


levelWin : String
levelWin =
  players1and2.fill


audioStartRampDuration : Float
audioStartRampDuration =
  1.2


audioTransitionDuration : Float
audioTransitionDuration =
  1.2


audioMuteDuration : Float
audioMuteDuration =
  0.6


audioUnmuteDuration : Float
audioUnmuteDuration =
  1.2
